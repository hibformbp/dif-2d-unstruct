function [T,D]=Reconstruction2ndOrder(stencil_cells,stencil_faces,stencil_size,ponderado)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   26 de Setembro de 2016                                          %
%                                                                                                   %
% Função que implementa a Solução Numérica a partir do Minimos Quadrados de 2ª Ordem                %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
global L Lref cell_side vert_side cell_num face_num vert_num w;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert cell_norm;
global face_bound cell_bound face_cells vert_cells;
global phi lap_phi  phi_faces flux_phi_faces;
%
for i=1:face_num
    %
    face=i;
    %
    %% Construção da Matriz D para cada Face, Celulas do Stencil %%
    n_cell=stencil_size(face,2);
    %
    for j=1:n_cell
        celula=stencil_cells(face,j);
        %
        if ponderado
            dx=(cells(celula,1)-faces(face,1));
            dy=(cells(celula,2)-faces(face,2));
            w=1/(sqrt(dx^2+dy^2))^2;
        else
            w=1;
        end
        %
        % Matriz D tendo em conta a ponderação %
        D1{face}(j,1)=w*1;
        %
        D1{face}(j,2)=w*(cells(celula,1)-faces(face,1));
        D1{face}(j,3)=w*(cells(celula,2)-faces(face,2));
        %
        % Matriz D %
        D{face}(j,1)=1;
        %
        D{face}(j,2)=(cells(celula,1)-faces(face,1));
        D{face}(j,3)=(cells(celula,2)-faces(face,2));
    end
    %
    %% Construção da Matriz D para cada Face, Faces do Stencil %%
    n_face=stencil_size(face,3);
    if n_face~=0
        for j=1:n_face
            facestencil=stencil_faces(face,j);
            %
            if ponderado
                if facestencil~=face
                    dx=(faces(facestencil,1)-faces(face,1));
                    dy=(faces(facestencil,2)-faces(face,2));
                else
                    aux=face_cells(face,1);
                    dx=(cells(aux,1)-faces(face,1));
                    dy=(cells(aux,2)-faces(face,2));
                end
                w=1/(sqrt(dx^2+dy^2))^2;
            else
                w=1;
            end
            %
            % Matriz D tendo em conta a ponderação %
            D1{face}(n_cell+j,1)=w*1;
            %
            D1{face}(n_cell+j,2)=w*(faces(facestencil,1)-faces(face,1));
            D1{face}(n_cell+j,3)=w*(faces(facestencil,2)-faces(face,2));
            %
            % Matriz D %
            D{face}(n_cell+j,1)=1;
            %
            D{face}(n_cell+j,2)=(faces(facestencil,1)-faces(face,1));
            D{face}(n_cell+j,3)=(faces(facestencil,2)-faces(face,2));
        end
    end
    %
    %% Determinação da Matriz T %%
    %
    % D*c=phi -> c=inv(D'D)*D'*phi -> T=inv(D'D)*D' %
    T{face}=inv(D{face}'*D1{face})*D1{face}';
end
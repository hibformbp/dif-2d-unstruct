function f=SolutionDiffusion(solution,x,y,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   28 de Junho de 2016                                             %
%                                  12 de Outubro de 2016                                            %
%                                                                                                   %
% Função que determina as soluções analiticas                                                       %
%                                                                                                   %
% solution - Função Analitica que se pretende calcular                                              %
% x        - Coordenada X                                                                           %
% y        - Coordenada Y                                                                           %
% type     - Escolhe entre a solução analitica 'anal', laplaciano 'lap' e as derivadas em x e y     %
%            'xflux' ou 'yflux'                                                                     %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
if strcmp(solution,'sin')==1
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=sin(3*pi*x)*sin(3*pi*y);
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=-18*pi^2*sin(3*pi*x)*sin(3*pi*y);
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=3*pi*cos(3*pi*x)*sin(3*pi*y);
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=3*pi*sin(3*pi*x)*cos(3*pi*y);
    end
elseif strcmp(solution,'exp')==1
    %
    % Constantes %
    s=sqrt(0.0175);
    u=0.5;
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=exp(-((x-u)^2+(y-u)^2)/s^2);
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=(-2/s^2+4*(x-u)^2/s^4+4*(y-u)^2/s^4-2/s^2)*exp(-((x-u)^2+(y-u)^2)/s^2);
        %-4/s^2*(1-((x-u)^2+(y-u)^2)/s^2)*exp(((x-u)^2+(y-u)^2)/s^2);
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=-2*(x-u)/s^2*exp(((x-u)^2+(y-u)^2)/s^2);
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=-2*(y-u)/s^2*exp(((x-u)^2+(y-u)^2)/s^2);
    end
elseif strcmp(solution,'2nd')==1
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=1/2*(x+y);
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=0;
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=1/2;
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=1/2;
    end
elseif strcmp(solution,'4th')==1
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=(y-1)^3+(x-1)^3;
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=6*(y-1)+6*(x-1);
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=3*(x-1)^2;
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=3*(y-1)^2;
    end
elseif strcmp(solution,'6th')==1
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=x^5;
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=20*x^3;
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=5*x^4;
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=0;
    end
elseif strcmp(solution,'8th')==1
    %
    % Solução Analitica %
    if strcmp(type,'anal')==1
        f=y^3;
    end
    %
    % Laplaciano %
    if strcmp(type,'lap')==1
        f=6*y;
    end
    %
    % Derivada X %
    if strcmp(type,'xflux')==1
        f=0;
    end
    %
    % Derivada Y %
    if strcmp(type,'yflux')==1
        f=3*y^2;
    end
end
### Description:

A Very High-Order Finite Volume Method Based on Weighted Least Squares for the Solution of Poisson Equation on Unstructured Grids

### References:

Vasconcelos AGR. “A Very High-Order Finite Volume Method Based on Weighted Least Squares for the Solution of Poisson Equation on Unstructured Grids”. MSc thesis. Instituto Superior Técnico, Universidade de Lisboa, 2017. URL: https://fenix.tecnico.ulisboa.pt/cursos/meaer/dissertacao/1972678479053984

Vasconcelos AGR, Albuquerque DMS, Pereira JCF. "A very high-order finite volume method based on weighted least squares for elliptic operators on polyhedral unstructured grids". In: Computers & Fluids 181 (2019), pp. 383-402. ISSN : 0045-7930. DOI: https://doi.org/10.1016/j.compfluid.2019.02.004.

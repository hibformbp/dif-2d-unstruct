function a=plots
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   04 de Julho de 2016                                             %
%                                                                                                   %
% Função que cria os graficos do problema                                                           %
%                                                                                                   %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert;
global face_bound cell_bound face_cells vert_cells;
global phi lap_phi  phi_faces flux_phi_faces;
global phi_num lap_phi_num;
global norma1_phi erro_phi_max erro_phi norma1_lap erro_lap_max erro_lap;
%
%% Variaveis Auxiliares %%
% Malha %
k=0;
for j=1:cell_side
    for i=1:cell_side
        k=k+1;
        X(i,j)=cells(k,1);
        Y(i,j)=cells(k,2);
    end
end
% Valores Analiticos %
k=0;
for i=1:cell_side
    for j=1:cell_side
        k=k+1;
        Z1(i,j)=phi(k);
        Z2(i,j)=lap_phi(k);
    end
end
%% Plots %%
%
% Phi Analitico %
figure (1)
a=contourf(X,Y,Z1)
%legend('\phi_{analitico}');
title('\phi_{analitico}');
xlabel('X');
ylabel('Y');
c=colorbar;
c.Label.String = '\phi_{analitico}';
% Laplaciano Analitico %
figure (2)
contourf(X,Y,Z2)
%legend('\nabla^2\phi_{analitico}');
title('\nabla^2\phi_{analitico}');
xlabel('X');
ylabel('Y');
colorbar;
%

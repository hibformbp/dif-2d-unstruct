clear all
clc
%
%% Declaração de Variavies %%
%
global TempoGlobal tempo_malha tempo_anal tempo_num tempo_A tempo_stencil tempo_rec tempo_gmres tempo_erro tempo_plots;
global fid;
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells faces;
global cell_verts cell_faces cell_vol cell_norm cell_bound cell_vert_num cell_face_num;
global face_vert face_cells face_area face_bound;
global vert_cells vert_cell_num vert_face_num;
global phi lap_phi  phi_faces flux_phi_faces;
global phi_num lap_phi_num A source source_faces source_cells stencil_cells stencil_faces stencil_size T D;
global norma1_phi erro_phi_max erro_phi norma1_lap erro_lap_max erro_lap X;
%
TempoGlobal=cputime;
%
%% Dados de Input %%
%
malha='cart';                               % Tipo de Malha ------------> cart-->Cartesiana  tri-->Triangular
solution='sin';                             % Solução Numerica que se está a usar -> sin--->sin(3*pi*x)             exp--->exp(-(x-mu)^2/s) %
equation='diffusion';                       % Equação que se pretende resolver
metodo='WLS_2';                             % Metedo Numerico ---------------------> FDM_2->2ª Ordem                WLS_2->Método de 2ª Ordem    WLS_4->Método de 4ª Ordem WLS_6->Método de 6ª Ordem    WLS_8->Método de 8ª Ordem %
%
uniforme=true;                              % Tipo de Malha Cartesiana -> treu-->Uniforme    false-->Não Uniforme
explicito=false;                            % Tipo de Cálculo ---------------------> true-->Cálculo Explicito       false->Cálculo Implicito %
dirichlet=true;                             % Condição de Fronteira ---------------> true-->Dirichlet               false->Neumann %
ponderado=true;                             % Ponderação nos Minimos Quadrados-----> true-->Ponderação              false->Sem Ponderação w=1;
GMRES=true;                                 % Solver ------------------------------> true-->GMRES                   false->BICGSTAB %
ILU=true;                                   % Pre Condicionador -------------------> true-->Precondicionador ILU    false->Não usa Precondicionador %
plots=false;                                 % Plot dos Resultados %
%
L=1;
%
fid=fopen('resultados.txt','w');
%
cell_side=10;
vert_side=cell_side+1;
%
%% Prints da Informação %%
%
informacao(malha,solution,equation,metodo,explicito,uniforme,dirichlet,ponderado,GMRES,ILU);
%
%% Geração da Malha %%
%
fprintf('\n\nConstrução da Malha\t\t\t\t\t\t Inicio ... ');
fprintf(fid,'\n\nConstrução da Malha\t\t\t\t\t\t Inicio ... ');
tempo_malha=cputime;
%
if strcmp(malha,'cart')==1
    [verts,cells,faces,cell_verts,cell_faces,cell_vol,face_area,face_vert,cell_norm,cell_num,vert_num,face_num,Lref]=CartMesh1(uniforme);
    [face_cells,vert_cells,face_bound,cell_bound,cell_vert_num, cell_face_num,vert_cell_num,vert_face_num]=CartMesh2;
elseif strcmp(malha,'tr')==1
    error('\n\n\tERRO: Função Indisponivel\n\n');
else
    error('\n\n\tERRO: Malha Desconhecida\n\n');
end
%
tempo_malha=cputime-tempo_malha;
tempo_total=cputime-TempoGlobal;
%
fprintf('%f ... %f ',tempo_malha,tempo_total);
fprintf('... Fim\n');
fprintf(fid,'%f ... %f ',tempo_malha,tempo_total);
fprintf(fid,'... Fim\n');
%
%% Solução Analitica %%
%
fprintf('\n\nCalculo dos Valores Analiticos\t\t\t Inicio ... ');
fprintf(fid,'\n\nCalculo dos Valores Analiticos\t\t\t Inicio ... ');
tempo_anal=cputime;
%
[phi,lap_phi,phi_faces,flux_phi_faces]=AnalyticalSolution(solution,metodo,equation);
%
tempo_anal=cputime-tempo_anal;
tempo_total=cputime-TempoGlobal;
%
fprintf('%f ... %f ',tempo_anal,tempo_total);
fprintf('... Fim\n');
fprintf(fid,'%f ... %f ',tempo_anal,tempo_total);
fprintf(fid,'... Fim\n');
%
%% Solução Numérica
%
fprintf('\n\nCálculo dos Valores Numericos\t\t\t Inicio ... |||||||| ... |||||||| ... |||\n');
fprintf(fid,'\n\nCálculo dos Valores Numericos\t\t\t Inicio ... |||||||| ... |||||||| ... |||\n');
tempo_num=cputime;
%
if metodo=='FDM_2'
    if strcmp(malha,'cart')==1
        [phi_num,lap_phi_num,A,source,source_faces,tempo_A,tempo_gmres]=FiniteDiferenceMethod2ndOrder(explicito,dirichlet,equation,GMRES,ILU);
    else
        error('\n\nERRO: Não Implentado para Malhas Triangulares\n\n');
    end
elseif metodo=='WLS_2'
    order=2;
    [phi_num,lap_phi_num,A,source,source_faces,source_cells,stencil_cells,stencil_faces,stencil_size,T,D,tempo_stencil,tempo_rec, tempo_A,tempo_gmres]=WeightedLeastSquares(order,dirichlet,equation,GMRES,ILU,ponderado);
elseif metodo=='CDM_2'
    error('\n\n\tERRO: Método Não Implementado\n\n');
    %[phi_num,lap_phi_num,A,Aw,source,sourcew,source_faces,stencil_cells,stencil_faces,stencil_size,T,D]=DC2ndOrder(dirichlet);
elseif metodo=='WLS_4'
    order=4;
    [phi_num,lap_phi_num,A,source,source_faces,source_cells,stencil_cells,stencil_faces,stencil_size,T,D,tempo_stencil,tempo_rec, tempo_A,tempo_gmres]=WeightedLeastSquares(order,dirichlet,equation,GMRES,ILU,ponderado);
elseif metodo=='WLS_6'
    order=6;
    [phi_num,lap_phi_num,A,source,source_faces,source_cells,stencil_cells,stencil_faces,stencil_size,T,D,tempo_stencil,tempo_rec, tempo_A,tempo_gmres]=WeightedLeastSquares(order,dirichlet,equation,GMRES,ILU,ponderado);
elseif metodo=='WLS_8'
    order=8;
    [phi_num,lap_phi_num,A,source,source_faces,source_cells,stencil_cells,stencil_faces,stencil_size,T,D,tempo_stencil,tempo_rec, tempo_A,tempo_gmres]=WeightedLeastSquares(order,dirichlet,equation,GMRES,ILU,ponderado);
else
    error('\n\n\tERRO: Método Desconhecido\n\n');
end
pn=phi_num; lpn=lap_phi_num; a=A; s=source; sf=source_faces; sc=source_cells; sz=stencil_size; t=T; d=D;
%
tempo_num=cputime-tempo_num;
tempo_total=cputime-TempoGlobal;
%
fprintf('\n\nCálculo dos Valores Numericos\t\t\t Inicio ... ');
fprintf('%f ... %f ',tempo_num, tempo_total);
fprintf('... Fim\n');
fprintf(fid,'\n\nCálculo dos Valores Numericos\t\t\t Inicio ... ');
fprintf(fid,'%f ... %f ',tempo_num, tempo_total);
fprintf(fid,'... Fim\n');
%%% Calculo do Erro %%
%
tempo_erro=cputime;
fprintf('\n\nCalculo do Erro\t\t\t\t\t\t\t Inicio ... ');
fprintf(fid,'\n\nCalculo do Erro\t\t\t\t\t\t\t Inicio ... ');
%
[norma1_phi,norma1_lap,erro_phi_max,erro_lap_max,erro_phi,erro_lap,X]=errorcalculation(explicito);
%
tempo_erro=cputime-tempo_erro;
tempo_total=cputime-TempoGlobal;
%
fprintf('%f ... %f ',tempo_erro, tempo_total);
fprintf('... Fim\n');
fprintf(fid,'%f ... %f ',tempo_erro, tempo_total);
fprintf(fid,'... Fim\n');
%
%% Plot dos Resultados %%
%
if plots
    fprintf('\n\nPlot dos Resultados\t\t\t\t\t\t Inicio ... ');
    fprintf(fid,'\n\nPlot dos Resultados\t\t\t\t\t\t Inicio ... ');
    tempo_plots=cputime;
    %
    plots;
    %
    tempo_plots=cputime-tempo_plots;
    tempo_total=cputime-TempoGlobal;
    %
    fprintf('%f ... %f ',tempo_plots, tempo_total);
    fprintf('... Fim\n');
    fprintf(fid,'%f ... %f ',tempo_plots, tempo_total);
    fprintf(fid,'... Fim\n');
else
    fprintf('\n\nPlot dos Resultados\t\t\t\t\t\t Inicio ...');
    fprintf('Não ... Fim\n');
    fprintf(fid,'\n\nPlot dos Resultados\t\t\t\t\t\t Inicio ...');
    fprintf(fid,'Não ... Fim\n');
end
%
%% Export dos Resultados %%
%
fprintf('\n\nExportar Resultados\t\t\t\t\t\t Inicio ... ');
fprintf(fid,'\n\nExportar Resultados\t\t\t\t\t\t Inicio ... ');
tempo_export=cputime;
%
exportplots(explicito);
%
tempo_export=cputime-tempo_export;
TempoGlobal=cputime-TempoGlobal;
%
fprintf('%f ... %f ',tempo_export, TempoGlobal);
fprintf('... Fim\n');
fprintf(fid,'%f ... %f ',tempo_export, TempoGlobal);
fprintf(fid,'... Fim\n');
%
%% Apresentação de Resultados %%
%
fprintf('\n\nResultados\n');
fprintf('\t\t\tn=%d\tLref=%E\tnorma1_phi=%E\tnormamax_phi=%E\t%f %f\n',cell_num,Lref,norma1_phi,erro_phi_max,X(2,1),X(2,2));
fprintf('\t\t\tn=%d\tLref=%E\tnorma1_lap=%E\tnormamax_lap=%E\t%f %f\n',cell_num,Lref,norma1_lap,erro_lap_max,X(1,1),X(1,2));
fprintf('\t\t\tTempo Global de Execução\t\t%E\n',TempoGlobal);
fprintf('Fim de Execução \n');
%
fprintf(fid,'\n\nResultados\n');
fprintf(fid,'\t\t\tn=%d\tLref=%E\tnorma1_phi=%E\tnormamax_phi=%E\t%f %f\n',cell_num,Lref,norma1_phi,erro_phi_max,X(2,1),X(2,2));
fprintf(fid,'\t\t\tn=%d\tLref=%E\tnorma1_lap=%E\tnormamax_lap=%E\t%f %f\n',cell_num,Lref,norma1_lap,erro_lap_max,X(1,1),X(1,2));
fprintf(fid,'\t\t\tTempo Global de Execução\t\t%E\n',TempoGlobal);
fprintf(fid,'Fim de Execução \n');
%
fclose(fid);

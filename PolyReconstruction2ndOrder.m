function f=PolyReconstruction2ndOrder(C,x,y,xc,yc,type)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                  14 de Outubro de 2016                                            %
%                                  14 de Outubro de 2016                                            %
%                                                                                                   %
% Função calcula a contirbuição de cada celula para a reconstrução do polinomio na face que se esta %
% a trabalhar, esta função tem como objectivo facilitar a construção da matriz A.                   %
% Atenção que esta função não determina o polinomio reconstruido na face, serve apenas para quando  %
% se esta a determinar as entradas na matriz A.                                                     %
% Caso queira saber o polinomio da face tenho de usar outra função                                  %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
if strcmp(type,'poly')==1
    f=C(1)+C(2)*(x-xc)+C(3)*(y-yc);
elseif strcmp(type,'xflux')==1
    f=C(2);
elseif strcmp(type,'yflux')==1
    f=C(3);
else
    error('\n\n\tERRO: Fluxo Desconhecido\n\n');
end
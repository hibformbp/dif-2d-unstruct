function [A,source,source_faces,source_cells]=MatrixDiffusion(dirichlet,order,T,G,stencil_cells,stencil_faces,stencil_size)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   26 de Setembro de 2016                                          %
%                                                                                                   %
% Função que implementa a Solução Numérica a partir do Minimos Quadrados de 2ª Ordem                %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
global TempoGlobal fid;
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells faces;
global cell_verts cell_faces cell_vol cell_norm cell_bound cell_vert_num cell_face_num;
global face_vert face_cells face_area face_bound;
global vert_cells vert_cell_num vert_face_num;
global phi lap_phi  phi_faces flux_phi_faces;
%
% Inicialização da Matriz A -> Não esquecer que no fim tenho de converter a Matriz A para uma matriz
% esparsa %
A=zeros(cell_num);
%
for i=1:cell_num
    if rem(i*10,cell_num)==0
        fprintf(' %d%% ',i*100/cell_num);
    end
    %
    % Celula Selecionada %
    cell=i;
    source_faces(cell)=0;
    %
    if dirichlet
        %
        % Faces da Célula %
        for j=1:cell_face_num(cell)
            %
            % Face Selecionada %
            face=cell_faces(cell,j);
            %
            % Coordenadas da Face
            xf=faces(face,1);
            yf=faces(face,2);
            %
            % Area da Face %
            area=face_area(face,1);
            %
            % Normal Exterior da Face %
            nx=cell_norm(cell,2*j-1);
            ny=cell_norm(cell,2*j);
            %
            % Pontos de Gauss da Face %
            if order==2
                x1=xf;
                y1=yf;
                P1=1;
            elseif order==4
                % Ponto de Gauss 1 %
                x1=G{face}(1,1);
                y1=G{face}(1,2);
                P1=G{face}(1,3);
                %
                % Ponto de Gauss 2 %
                x2=G{face}(2,1);
                y2=G{face}(2,2);
                P2=G{face}(2,3);
                %    
            elseif order==6
                % Ponto de Gauss 1 %
                x1=G{face}(1,1);
                y1=G{face}(1,2);
                P1=G{face}(1,3);
                %
                % Ponto de Gauss 2 %
                x2=G{face}(2,1);
                y2=G{face}(2,2);
                P2=G{face}(2,3);
                %
                % Ponto de Gauss 3 %
                x3=G{face}(3,1);
                y3=G{face}(3,2);
                P3=G{face}(3,3);
            elseif order==8
                % Ponto de Gauss 1 %
                x1=G{face}(1,1);
                y1=G{face}(1,2);
                P1=G{face}(1,3);
                %
                % Ponto de Gauss 2 %
                x2=G{face}(2,1);
                y2=G{face}(2,2);
                P2=G{face}(2,3);
                %
                % Ponto de Gauss 3 %
                x3=G{face}(3,1);
                y3=G{face}(3,2);
                P3=G{face}(3,3);
                %
                % Ponto de Gauss 4 %
                x4=G{face}(4,1);
                y4=G{face}(4,2);
                P4=G{face}(4,3);
            end
            %
            for k=1:stencil_size(face,2)
                %
                % Celulas do Stencil %
                cell_aux=stencil_cells(face,k);
                %
                % Constantes %
                C=T{face}(:,k);
                %
                if order==2
                    %
                    % 2ª Ordem %
                    aux_x1=PolyReconstruction2ndOrder(C,x1,y1,xf,yf,'xflux');
                    aux_y1=PolyReconstruction2ndOrder(C,x1,y1,xf,yf,'yflux');
                    %
                    aux_x=aux_x1*area*nx;
                    aux_y=aux_y1*area*ny;
                elseif order==4
                    %
                    % 4ª Ordem %
                    aux_x1=PolyReconstruction4thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction4thOrder(C,x2,y2,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction4thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction4thOrder(C,x2,y2,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2)*area*nx;
                    aux_y=(aux_y1*P1+aux_y2*P2)*area*ny;
                elseif order==6
                    %
                    % 6ª Ordem %
                    aux_x1=PolyReconstruction6thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction6thOrder(C,x2,y2,xf,yf,'xflux');
                    aux_x3=PolyReconstruction6thOrder(C,x3,y3,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction6thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction6thOrder(C,x2,y2,xf,yf,'yflux');
                    aux_y3=PolyReconstruction6thOrder(C,x3,y3,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2+aux_x3*P3)*area*nx;
                    aux_y=(aux_y1*P1+aux_y2*P2+aux_y3*P3)*area*ny;
                elseif order==8
                    %
                    % 8ª Ordem
                    aux_x1=PolyReconstruction8thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction8thOrder(C,x2,y2,xf,yf,'xflux');
                    aux_x3=PolyReconstruction8thOrder(C,x3,y3,xf,yf,'xflux');
                    aux_x4=PolyReconstruction8thOrder(C,x4,y4,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction8thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction8thOrder(C,x2,y2,xf,yf,'yflux');
                    aux_y3=PolyReconstruction8thOrder(C,x3,y3,xf,yf,'yflux');
                    aux_y4=PolyReconstruction8thOrder(C,x4,y4,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2+aux_x3*P3+aux_x4*P4)*area*nx;
                    aux_y=(aux_y1*P1+aux_y2*P2+aux_y3*P3+aux_y4*P4)*area*ny;
                else
                    error('\n\nERRO: Ordem Não Implementada\n\n');
                end
                %
                A(cell,cell_aux)=A(cell,cell_aux)+(aux_x+aux_y);
            end
            %
            z=stencil_size(face,2);
            for k=1:stencil_size(face,3)
                %
                % Face do Stencil %
                face_aux=stencil_faces(face,k);
                %
                % Constantes %
                C=T{face}(:,z+k);
                %
                if order==2
                    %
                    aux_x1=PolyReconstruction2ndOrder(C,x1,y1,xf,yf,'xflux');
                    aux_y1=PolyReconstruction2ndOrder(C,x1,y1,xf,yf,'yflux');
                    %
                    aux_x=aux_x1*area*nx*phi_faces(face_aux);
                    aux_y=aux_y1*area*ny*phi_faces(face_aux);
                elseif order==4
                    %
                    % 4ª Ordem %
                    aux_x1=PolyReconstruction4thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction4thOrder(C,x2,y2,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction4thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction4thOrder(C,x2,y2,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2)*area*nx*phi_faces(face_aux);
                    aux_y=(aux_y1*P1+aux_y2*P2)*area*ny*phi_faces(face_aux);
                elseif order==6
                    %
                    % 6ª Ordem %
                    aux_x1=PolyReconstruction6thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction6thOrder(C,x2,y2,xf,yf,'xflux');
                    aux_x3=PolyReconstruction6thOrder(C,x3,y3,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction6thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction6thOrder(C,x2,y2,xf,yf,'yflux');
                    aux_y3=PolyReconstruction6thOrder(C,x3,y3,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2+aux_x3*P3)*area*nx*phi_faces(face_aux);
                    aux_y=(aux_y1*P1+aux_y2*P2+aux_y3*P3)*area*ny*phi_faces(face_aux);
                elseif order==8
                    %
                    % 8ª Ordem
                    aux_x1=PolyReconstruction8thOrder(C,x1,y1,xf,yf,'xflux');
                    aux_x2=PolyReconstruction8thOrder(C,x2,y2,xf,yf,'xflux');
                    aux_x3=PolyReconstruction8thOrder(C,x3,y3,xf,yf,'xflux');
                    aux_x4=PolyReconstruction8thOrder(C,x4,y4,xf,yf,'xflux');
                    %
                    aux_y1=PolyReconstruction8thOrder(C,x1,y1,xf,yf,'yflux');
                    aux_y2=PolyReconstruction8thOrder(C,x2,y2,xf,yf,'yflux');
                    aux_y3=PolyReconstruction8thOrder(C,x3,y3,xf,yf,'yflux');
                    aux_y4=PolyReconstruction8thOrder(C,x4,y4,xf,yf,'yflux');
                    %
                    aux_x=(aux_x1*P1+aux_x2*P2+aux_x3*P3+aux_x4*P4)*area*nx*phi_faces(face_aux);
                    aux_y=(aux_y1*P1+aux_y2*P2+aux_y3*P3+aux_y4*P4)*area*ny*phi_faces(face_aux);
                else
                    error('\n\nERRO: Ordem Não Implementada\n\n');
                end
                %
                source_faces(cell)=source_faces(cell)+(aux_x+aux_y);
            end
        end
    else
        error('\n\nERRO:Condição de Fronteira Não Implementada\n\n');
    end
    %
    source_cells(cell)=lap_phi(cell)*cell_vol(cell);
    source(cell)=source_cells(cell)-source_faces(cell);
end
%
fprintf(' ... Fim\n');
%
A=sparse(A);
%
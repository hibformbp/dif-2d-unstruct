function exportplots(explicito)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               Artur Guilherme Vasconcelos                                         %
%                                   23 de Setembro de 2016                                          %
%                                                                                                   %
% Esta função exporta os resultados para um ficheiro, results.dat, cujo formato está implementado de% 
% forma a que seja possivel exporta-los para o TecPlot de forma a que se possa obter as             %
% distribuiçoes do erro e das proprieades que se esta a calcular.                                   %
%                                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%% Declaração de Variavies %%
%
global L Lref cell_side vert_side cell_num face_num vert_num;
global verts cells cell_verts cell_faces cell_vol faces face_area face_vert;
global face_bound cell_bound face_cells vert_cells;
global phi lap_phi  phi_faces flux_phi_faces;
global phi_num lap_phi_num A source;
global norma1_phi erro_phi_max erro_phi norma1_lap erro_lap_max erro_lap;
%
fid=fopen('results.dat','wt');
%
%% Escrita do Cabeçalho %%
%
fprintf(fid,'TITLE= "results"\n');
fprintf(fid,'VARIABLES= "X", "Y", "Z", "PHI", "LAP", "PHI_NUM", "LAP_NUM" "ERRO_PHI" "ERRO_LAP"\n');
fprintf(fid,'ZONE N=%d E=%d, ZONETYPE=FEBRICK, DATAPACKING=BLOCK, T="P0BL0_cartesian"\n',vert_num*2,cell_num); % N - Vertices, E - Elementos %
fprintf(fid,'VARLOCATION= ([1-3]=NODAL, [4-9]=CELLCENTERED)\n');
%
%% Escrita dos Vertices da Malha %%
%
count=0;
count_line=20;
%
% Coordenadas XX %
for i=1:vert_num
    %
    count=count+1;
    fprintf(fid,'%e %e ',verts(i,1),verts(i,1));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
% Coordenadas Y %
for i=1:vert_num
    %
    count=count+1;
    fprintf(fid,'%e %e ',verts(i,2),verts(i,2));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
% Coordenadas Z %
for i=1:vert_num
    %
    count=count+1;
    fprintf(fid,'0 1 ');
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Phi Analitico %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',phi(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Laplaciano Analitico %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',lap_phi(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Phi Numérico %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',phi_num(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Laplaciano Numerico %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',lap_phi_num(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Erro Phi %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',erro_phi(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Escrita dos Resultados - Erro Laplaciano %%
%
count=0;
%
for i=1:cell_num
    %
    count=count+1;
    fprintf(fid,'%e ',erro_lap(i));
    %
    if count>count_line
        count=0;
        fprintf(fid,'\n');
    end
end
%
count=0;
fprintf(fid,'\n');
%
%% Não sei para que serve %%
%
for i=1:cell_side
    for j=1:cell_side
        %
        ie=1+(j-1)*2+(i-1)*(cell_side+1)*2;
        se=ie+2;
        id=1+(j-1)*2+(i)*(cell_side+1)*2;
        sd=id+2;
        %
        fprintf(fid,'%d %d %d %d ',se,ie,ie+1,se+1);
        fprintf(fid,'%d %d %d %d\n',sd,id,id+1,sd+1);
    end
end
fclose(fid);
%
end